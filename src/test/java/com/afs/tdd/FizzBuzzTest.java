package com.afs.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FizzBuzzTest {
    @Test
    void should_return_normal_when_fizzBuzz_given_input_normal() {
        // Given
        FizzBuzz fb = new FizzBuzz();
        Integer normalInput = 2;


        // When
        String output = fb.fizzBuzz(normalInput);

        // Then
        assertEquals("2", output);

    }

    @Test
    void should_return_fizz_when_fizzBuzz_given_input_3() {
        // Given
        FizzBuzz fb = new FizzBuzz();
        Integer input_3 = 3;

        // When
        String output = fb.fizzBuzz(input_3);

        // Then
        assertEquals("fizz", output);

    }

    @Test
    void should_return_fizz_when_fizzBuzz_given_input_multiple_of_3() {
        // Given
        FizzBuzz fb = new FizzBuzz();
        Integer input_6 = 6;

        // When
        String output = fb.fizzBuzz(input_6);

        // Then
        assertEquals("fizz", output);

    }

    @Test
    void should_return_buzz_when_fizzBuzz_given_input_5() {
        // Given
        FizzBuzz fb = new FizzBuzz();
        Integer input_5 = 5;

        // When
        String output = fb.fizzBuzz(input_5);

        // Then
        assertEquals("buzz", output);

    }

    @Test
    void should_return_buzz_when_fizzBuzz_given_input_multiple_of_5() {
        // Given
        FizzBuzz fb = new FizzBuzz();
        Integer input_10 = 10;

        // When
        String output = fb.fizzBuzz(input_10);

        // Then
        assertEquals("buzz", output);

    }

    @Test
    void should_return_fizzbuzz_when_fizzBuzz_given_input_15() {
        // Given
        FizzBuzz fb = new FizzBuzz();
        Integer input_15 = 15;

        // When
        String output = fb.fizzBuzz(input_15);

        // Then
        assertEquals("fizzbuzz", output);

    }

    @Test
    void should_return_fizzbuzz_when_fizzBuzz_given_input_multiple_of_3_and_5() {
        // Given
        FizzBuzz fb = new FizzBuzz();
        Integer input_15 = 30;

        // When
        String output = fb.fizzBuzz(input_15);

        // Then
        assertEquals("fizzbuzz", output);

    }
    // ==== New Reqs test ====
    @Test
    void should_return_whizz_when_fizzBuzz_given_input_7() {
        // Given
        FizzBuzz fb = new FizzBuzz();
        Integer input_7 = 7;

        // When
        String output = fb.fizzBuzz(input_7);

        // Then
        assertEquals("whizz", output);

    }

    @Test
    void should_return_whizz_when_fizzBuzz_given_input_multiple_of_7() {
        // Given
        FizzBuzz fb = new FizzBuzz();
        Integer input_28 = 28;

        // When
        String output = fb.fizzBuzz(input_28);

        // Then
        assertEquals("whizz", output);

    }

    @Test
    void should_return_fizzwhizz_when_fizzBuzz_given_input_21() {
        // Given
        FizzBuzz fb = new FizzBuzz();
        Integer input_21 = 21;

        // When
        String output = fb.fizzBuzz(input_21);


        // Then
        assertEquals("fizzwhizz", output);

    }

    @Test
    void should_return_fizzwhizz_when_fizzBuzz_given_input_multiple_of_3_and_7() {
        // Given
        FizzBuzz fb = new FizzBuzz();
        Integer input_multiple_3_7 = 42;

        // When
        String output = fb.fizzBuzz(input_multiple_3_7);

        // Then
        assertEquals("fizzwhizz", output);

    }

    @Test
    void should_return_buzzwhizz_when_fizzBuzz_given_input_35() {
        // Given
        FizzBuzz fb = new FizzBuzz();
        Integer input_35 = 35;

        // When
        String output = fb.fizzBuzz(input_35);

        // Then
        assertEquals("buzzwhizz", output);

    }

    @Test
    void should_return_buzzwhizz_when_fizzBuzz_given_input_multiple_of_5_and_7() {
        // Given
        FizzBuzz fb = new FizzBuzz();
        Integer input_multiple_5_7 = 70;


        // When
        String output = fb.fizzBuzz(input_multiple_5_7);

        // Then
        assertEquals("buzzwhizz", output);

    }

    @Test
    void should_return_fizzbuzzwhizz_when_fizzBuzz_given_input_105() {
        // Given
        FizzBuzz fb = new FizzBuzz();
        Integer input_105 = 105;


        // When
        String output = fb.fizzBuzz(input_105);

        // Then
        assertEquals("fizzbuzzwhizz", output);

    }

    @Test
    void should_return_fizzbuzzwhizz_when_fizzBuzz_given_input_multiple_of_3_5_7() {
        // Given
        FizzBuzz fb = new FizzBuzz();
        Integer input_multiple_all_three = 420;

        // When
        String output = fb.fizzBuzz(input_multiple_all_three);

        // Then
        assertEquals("fizzbuzzwhizz", output);

    }




}
