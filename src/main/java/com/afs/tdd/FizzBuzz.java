package com.afs.tdd;

public class FizzBuzz {


    public String fizzBuzz(Integer input) {
        if (input % 3 == 0 && input % 5 == 0 && input % 7 == 0) {
            return "fizzbuzzwhizz";
        } else if (input % 3 == 0 && input % 5 == 0) {
            return "fizzbuzz";
        } else if (input % 3 == 0 && input % 7 == 0) {
            return "fizzwhizz";
        } else if (input % 5 == 0 && input % 7 == 0) {
            return "buzzwhizz";
        } else if (input % 3 == 0) {
            return "fizz";
        } else if (input % 5 == 0) {
            return "buzz";
        } else if (input % 7 == 0) {
            return "whizz";
        }
        return input.toString();
    }
}